(function() {
	'use strict';
	angular.module('angular_btap').controller('AppController', ['$scope', function AppController($scope) {

	}]);
	angular.module('angular_btap').controller('HomeController', ['$scope', '$location', function HomeController($scope, $location) {
		$scope.goToService = function() {
			$location.path('/services');
		};
	}]);
	angular.module('angular_btap').controller('ContactsController', ['$scope', '$location', function ContactsController($scope, $location) {
		$scope.goToService = function() {
			$location.path('/services');
		};
	}]);
	angular.module('angular_btap').controller('TeamController', ['$scope', '$location', function ContactsController($scope, $location) {
		$scope.goToService = function() {
			$location.path('/services');
		};
	}]);
	angular.module('angular_btap').controller('PortFolioController', ['$scope', '$location', '$http', '$filter', function RootController($scope, $location, $http, $filter) {
		var $container = $('#lightbox');
		$scope.userName = 'Venkatesan Rengasamy';
		var margin = {
				top: 20,
				right: 20,
				bottom: 30,
				left: 50
			},
			width = 960 - margin.left - margin.right,
			height = 500 - margin.top - margin.bottom;
		var parseDate = d3.time.format('%d-%b-%y').parse;

		var x = d3.time.scale()
			.range([0, width]);

		var y = d3.scale.linear()
			.range([height, 0]);

		var xAxis = d3.svg.axis()
			.scale(x)
			.orient('bottom');

		var yAxis = d3.svg.axis()
			.scale(y)
			.orient('left');

		var area = d3.svg.area()
			.x(function(d) {
				return x(d.date);
			})
			.y0(height)
			.y1(function(d) {
				return y(d.close);
			});

		$scope.plotGraph = function() {
			var svg = d3.select('#plotGraph').append('svg')
				.attr('width', width + margin.left + margin.right)
				.attr('height', height + margin.top + margin.bottom)
				.append('g')
				.attr('transform', 'translate(" + margin.left + "," + margin.top + ")');
			var data = [];
			var d = {};
			for (var i = -2000; i < 2000; i++) {
				var testData = {};
				testData.date = i;
				testData.close = 0.01 * i * i;
				data.push(testData);
			}
			angular.forEach(data, function(value, key) {
				data[key].date = value.date;
				data[key].close = +value.close;
			});

			x.domain(d3.extent(data, function(d) {
				return d.date;
			}));
			y.domain([0, d3.max(data, function(d) {
				return d.close;
			})]);

			svg.append('path')
				.datum(data)
				.attr('class', 'area')
				.attr('d', area);

			svg.append('g')
				.attr('class', 'x axis')
				.attr('transform', 'translate(0," + height + ")')
				.call(xAxis);

			svg.append('g')
				.attr('class', 'y axis')
				.call(yAxis)
				.append('text')
				.attr('transform', 'rotate(-90)')
				.attr('y', 6)
				.attr('dy', '.71em')
				.style('text-anchor', 'end')
				.text('index');
		};


		function animateAct(selector) {
			if ($container) {
				$container.isotope({
					filter: selector,
					animationOptions: {
						duration: 750,
						easing: 'linear',
						queue: false
					}
				});
			}

		}
		$scope.changeSelectedItems = function(selector, myThis) {
			$('.cat .active').removeClass('active');
			$(myThis).addClass('active');
			animateAct(selector);
			return false;
		};
		$scope.$on('$viewContentLoaded', function() {
			animateAct('*');
		});

	}]);
})(); // ends main function