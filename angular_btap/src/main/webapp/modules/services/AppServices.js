   (function() {
       'use strict';
       angular.module('angular_btap').factory('AppServices', ['$resource', 'serviceapi',
           function($resource, serviceapi) {
               return $resource('', {}, {
                   homeInfo: {
                       method: 'POST',
                       isArray: true,
                       url: serviceapi.GET_HOME_API_URL,
                       timeout: 100000
                   },
                   contactsInfo: {
                       method: 'GET',
                       isArray: false,
                       url: serviceapi.GET_CONTACTS_API_URL,
                       timeout: 100000
                   },
                   teamInfo: {
                       method: 'GET',
                       isArray: false,
                       url: serviceapi.GET_TEAM_API_URL,
                       timeout: 100000
                   },
                   servicesInfo: {
                       method: 'GET',
                       isArray: false,
                       url: serviceapi.GET_SERVICES_API_URL,
                       timeout: 100000
                   }
               });
           }
       ]);
   })(); // ends main function