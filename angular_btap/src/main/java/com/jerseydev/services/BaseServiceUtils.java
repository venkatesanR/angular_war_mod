package com.jerseydev.services;

import com.jerseydev.model.AboutPage;
import com.jerseydev.model.Contacts;
import com.jerseydev.model.HomeDetails;
import com.jerseydev.model.TeamServices;

public class BaseServiceUtils {
	private AboutPage about;
	private Contacts contacts;
	private HomeDetails home;
	private TeamServices team;

	public AboutPage getAbout() {
		return about;
	}

	public void setAbout(AboutPage about) {
		this.about = about;
	}

	public Contacts getContacts() {
		return contacts;
	}

	public void setContacts(Contacts contacts) {
		this.contacts = contacts;
	}

	public HomeDetails getHome() {
		return home;
	}

	public void setHome(HomeDetails home) {
		this.home = home;
	}


	public TeamServices getTeam() {
		return team;
	}

	public void setTeam(TeamServices team) {
		this.team = team;
	}

	public AboutPage getAboutInfo() {
		about.setMission("Mission - We deliver uniqueness and quality");
		about.setSkills("Skills - Delivering fast and excellent results");
		about.setClients("Clients - Satisfied clients thanks to our experience");
		return about;
	}
	public HomeDetails getHomeInfo() {
		home.setUserName("Venkatesan Rengasamy");
		home.setLabName("INNOVATION LAB");
		return home;
	}
}
