package com.jerseydev.providers;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.jerseydev.model.AboutPage;
import com.jerseydev.model.HomeDetails;
import com.jerseydev.services.BaseServiceUtils;
@Component
@Path("/myangular")
public class AngularAppRS {
	private static final Logger logger = Logger.getLogger(AngularAppRS.class);

	@Autowired
	@Qualifier("uiService")
	private BaseServiceUtils utility;

	@GET
	@Path("/home")
	@Produces(MediaType.APPLICATION_JSON)
	public HomeDetails getHomeDetails() {
		logger.info("//home RS Calling");
		return utility.getHomeInfo();
	}
	
	@GET
	@Path("/about")
	@Produces(MediaType.APPLICATION_JSON)
	public AboutPage getAboutDetails() {
		logger.info("//about RS Calling");
		return utility.getAboutInfo();
	}
}
