package com.jerseydev.providers;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;

public class BaseRestService extends ResourceConfig {
	public BaseRestService() {
		register(RequestContextFilter.class);
		packages("com.jerseydev.providers");
		register(JacksonFeature.class);
		register(RequestHandlers.class);
	}
}
